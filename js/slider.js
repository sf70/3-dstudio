var images = [
  {
    src: "./images/image1.png",
    name: "Rostov-on-Don, Admiral",
    city: "Rostov-on-Don LCD admiral",
    area: "81 m2",
    time: "3.5 months",
    cost: "Upon request",
  },
  {
    src: "./images/image2.png",
    name: "Sochi Thieves",
    city: "Sochi Thieves",
    area: "105 m2",
    time: "4 months",
    cost: "Upon request",
  },
  {
    src: "./images/image3.png",
    name: "Rostov-on-Don Patriotic",
    city: "Rostov-on-Don Patriotic",
    area: "93 m2",
    time: "3 months",
    cost: "Upon request",
  },
]


function initSlider() {
  var sliderImages = document.querySelector(".slider__pictures");
  var sliderArrows = document.querySelectorAll(".slider__arrow");
  var sliderArrowLeft = document.querySelector(".arrow__left");
  var sliderArrowRight = document.querySelector(".arrow__right");
  var sliderDots = document.querySelector(".dots");
  var sliderText = document.querySelector(".project__text");
  var sliderCity = document.querySelector(".project__city__div");
  var sliderArea = document.querySelector(".project__area__div");
  var sliderTime = document.querySelector(".project__time__div");
  var sliderCost = document.querySelector(".project__cost__div");
  var sliderLinks = document.querySelector(".objects__switch");
  
  initImages();
  initDots();
  initInfo();
  initLinks();
  // ===
  initArrows();
  
  function initImages() {
    images.forEach((image, index) => {
         var imageElement = `<img src="${image.src}" alt="object picture" class="project__main__img ${index === 0? "slider__pictures__active" : "slider__pictures__passive"} n${index}" data-index="${index}">`;
         sliderImages.innerHTML += imageElement;
       });
  }

  function initDots() {
    images.forEach((image, index) => {
          var dotElement = `<span class="dot n${index} ${index === 0? "dot__active" : ""} pagination__margin" data-index="${index}"></span>`;
          sliderDots.innerHTML += dotElement;
        });
    sliderDots.querySelectorAll(".dot").forEach(dot => {
      dot.addEventListener("click", function() {
        moveSlider(this.dataset.index);
      });
    });
  }

  function initLinks() {
    images.forEach((image, index) => {
      var linkElement = `<li class="position n${index} ${index === 0? "objects__active object__underline" : "objects__switcher__color_p objects"} objects__switcher__color" data-index="${index}">${image.name}</li>`;
      sliderLinks.innerHTML += linkElement;
      sliderLinks.querySelectorAll(".position").forEach(pos => {
        pos.addEventListener("click", function() {
          moveSlider(this.dataset.index);
        });
      });
    });


    // <li class="position objects__active object__underline"><a href="./index.html" class="objects__switcher__color">Rostov-on-Don, Admiral</a></li>
    //             <li class="position objects objects__switcher__color_p">Sochi Thieves</li>
  }

  function initInfo() {
    images.forEach((image, index) => {
      var cityElement = `<p class="project__subtext n${index} ${index === 0? "city__active" : "city__passive"}" data-index="${index}">${image.city}</p>`;
      sliderCity.innerHTML += cityElement;
      var areaElement = `<p class="project__subtext n${index} ${index === 0? "area__active" : "area__passive"}" data-index="${index}">${image.area.slice(0, -1)}<sup>${image.area.slice(-1)}</sup></p>`;
      sliderArea.innerHTML += areaElement;
      var timeElement = `<p class="project__subtext n${index} ${index === 0? "time__active" : "time__passive"}" data-index="${index}">${image.time}</p>`;
      sliderTime.innerHTML += timeElement;
      var costElement = `<p class="project__subtext n${index} ${index === 0? "cost__active" : "cost__passive"}" data-index="${index}">${image.cost}</p>`;
      sliderCost.innerHTML += costElement;
    });
  }

  function initArrows() {
    sliderArrows.forEach(arrow => {
      arrow.addEventListener("click", function() {
        var curNumber = +sliderImages.querySelector(".slider__pictures__active").dataset.index;
        var nextNumber;
        if (arrow.classList.contains("arrow__left")) {
          nextNumber = curNumber === 0? images.length - 1 : curNumber - 1;
        } else {
          nextNumber = curNumber === images.length - 1? 0 : curNumber + 1;
        }
        moveSlider(nextNumber);
      });
    });
  }









  
  function moveSlider(num) {
    // Move images
    var curImage = sliderImages.querySelector(".slider__pictures__active")
    curImage.classList.remove("slider__pictures__active")
    curImage.classList.add("slider__pictures__passive");
    sliderImages.querySelector(".n" + num).classList.remove("slider__pictures__passive");
    sliderImages.querySelector(".n" + num).classList.add("slider__pictures__active");
    // Move dots
    sliderDots.querySelector(".dot__active").classList.remove("dot__active");
    sliderDots.querySelector(".n" + num).classList.add("dot__active");
    // Move info
    var curCity= sliderCity.querySelector(".city__active")
    curCity.classList.remove("city__active");
    curCity.classList.add("city__passive");
    sliderCity.querySelector(".n" + num).classList.add("city__active");
    sliderCity.querySelector(".n" + num).classList.remove("city__passive");
    var curArea= sliderArea.querySelector(".area__active")
    curArea.classList.remove("area__active");
    curArea.classList.add("area__passive");
    sliderArea.querySelector(".n" + num).classList.add("area__active");
    sliderArea.querySelector(".n" + num).classList.remove("area__passive");
    var curTime= sliderTime.querySelector(".time__active")
    curTime.classList.remove("time__active");
    curTime.classList.add("time__passive");
    sliderTime.querySelector(".n" + num).classList.add("time__active");
    sliderTime.querySelector(".n" + num).classList.remove("time__passive");
    var curCost= sliderCost.querySelector(".cost__active")
    curCost.classList.remove("cost__active");
    curCost.classList.add("cost__passive");
    sliderCost.querySelector(".n" + num).classList.add("cost__active");
    sliderCost.querySelector(".n" + num).classList.remove("cost__passive");

    //Move links
    var curLink= sliderLinks.querySelector(".objects__active")
    curLink.classList.remove("objects__active");
    curLink.classList.remove("object__underline");
    curLink.classList.add("objects__switcher__color_p");
    curLink.classList.add("objects");
    sliderLinks.querySelector(".n" + num).classList.remove("objects__switcher__color_p");
    sliderLinks.querySelector(".n" + num).classList.remove("objects");
    sliderLinks.querySelector(".n" + num).classList.add("objects__active");
    sliderLinks.querySelector(".n" + num).classList.add("object__underline");


    // sliderImages.querySelector(".n" + num).classList.add("slider__pictures__active");
    // if (options.dots) {
    // }
    // if (options.titles) changeTitle(num);
  }








};






document.addEventListener("DOMContentLoaded", function() {
  initSlider();
});